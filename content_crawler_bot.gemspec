# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'content_crawler_bot/version'

Gem::Specification.new do |spec|
  spec.name          = "content_crawler_bot"
  spec.version       = ContentCrawlerBot::VERSION
  spec.authors       = ["Vlad Faust"]
  spec.email         = ["tech@vladfaust.com"]

  spec.summary       = %q{A core for Telegram Content Crawler Bots.}
  spec.homepage      = "https://github.com/vladfaust/content_crawler_bot"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency 'rake'
  spec.add_dependency 'roda'

  spec.add_dependency 'dotenv'
  spec.add_dependency 'telegram-bot-ruby'
  spec.add_dependency 'hanami-utils'
  spec.add_dependency 'blockchain-api'
  spec.add_dependency 'bitcoinpay'

  spec.add_dependency 'pg'
  spec.add_dependency 'sequel'
  spec.add_dependency 'redis'
  spec.add_dependency 'connection_pool'
  spec.add_dependency 'attribute_enum'
  spec.add_dependency 'interactor'

  spec.add_dependency 'resque'
  spec.add_dependency 'resque-scheduler'

  spec.add_dependency 'mono_logger'
  spec.add_dependency 'httparty'
  spec.add_dependency 'rqrcode'

  spec.add_dependency 'i18n'

  spec.add_development_dependency 'byebug'
  spec.add_development_dependency "bundler", "~> 1.12"
  spec.add_development_dependency "rake", "~> 10.0"
end
