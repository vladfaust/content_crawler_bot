module ContentCrawlerBot
  module Apps
    module Blockchain
      class CallbackHandler
        def call(env)
          request = Rack::Request.new env

          raise InvalidSecretError unless request.params['secret'] == ENV['BLOCKCHAIN_SECRET']

          result = ContentCrawlerBot::Interactors::Payments::Find.call(payment_id: request.params['payment_id'].to_i)
          payment = if result.success?
            result.payment
          else
            raise InvalidPaymentIDError
          end

          confirmations = request.params['confirmations'].to_i
          value = (request.params['value'].to_f / 100_000_000.0)

          ContentCrawlerBot::Interactors::Payments::Bitcoin::UpdateConfirmations.call(payment: payment, confirmations: confirmations)

          confirmations_needed = Settings.bitcoin_confirmations_needed.is_a?(Proc) ? Settings.bitcoin_confirmations_needed.call(value) : Settings.bitcoin_confirmations_needed
          raise NotEnoughConfirmationsError unless confirmations >= confirmations_needed
          raise SmallValueError if (value / payment.amount) < (1 - Settings.bitcoin_mercy)

          ContentCrawlerBot::Interactors::Payments::Complete.call(payment: payment, amount: value)

          [200, {}, ['*ok*']]

        rescue SmallValueError => e
          $logger.info("#{ e.class } with #{ request.params }")
          [200, {}, ['*ok*']]

        rescue NotEnoughConfirmationsError
          [402, {}, ['*need more confirmations*']]

        rescue InvalidSecretError, InvalidPaymentIDError, InvalidAddressError => e
          $logger.error("#{ e.class } with #{ request.params }")
          [200, {}, ['*ok*']]

        rescue Exception => e
          $logger.error("#{ e.message } at #{ e.backtrace }")
          [500, {}, ['Internal server error']]

        end

        class InvalidSecretError < StandardError; end
        class InvalidPaymentIDError < StandardError; end
        class InvalidAddressError < StandardError; end
        class NotEnoughConfirmationsError < StandardError; end
        class SmallValueError < StandardError; end
      end
    end
  end
end
