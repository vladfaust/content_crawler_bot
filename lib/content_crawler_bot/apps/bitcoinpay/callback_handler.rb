require 'bitcoinpay'
require 'json'

module ContentCrawlerBot
  module Apps
    module Bitcoinpay
      class CallbackHandler
        def call(env)
          request = Rack::Request.new env
          # TODO: implement SHA-256 protection
          body = request.body.read
          params = JSON.parse(body)

          reference = JSON.parse(params&.[]('reference'))
          raise InvalidSecretError unless reference['secret']&. == ENV['BITCOINPAY_SECRET']

          result = ContentCrawlerBot::Interactors::Payments::Find.call(
            payment_id: reference['payment_id'].to_i)
          payment = result.success? ? result.payment : (raise InvalidPaymentIDError)

          if params['status'] == 'confirmed'
            client = ::Bitcoinpay::Client.new(ENV['BITCOINPAY_API_KEY'])
            raise StatusesMismatchError unless client.get_payment(payment_id: params['payment_id'])['data']['status'] == 'confirmed'
            ContentCrawlerBot::Interactors::Payments::Complete.call(payment: payment)
          end

          [200, {}, ['*ok*']]

        rescue Exception => e
          $logger.error("#{ e.message } at #{ e.backtrace }")
          [500, {}, ['Internal server error']]

        end

        class InvalidSecretError < StandardError; end
        class InvalidPaymentIDError < StandardError; end
        class StatusesMismatchError < StandardError; end
      end
    end
  end
end
