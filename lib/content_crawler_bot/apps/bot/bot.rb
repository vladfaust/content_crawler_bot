require 'telegram/bot'
require 'singleton'
require_relative 'setup'

module ContentCrawlerBot
  module Apps
    class Bot
      include Singleton

      attr_reader :bot

      def initialize
        @bot = Telegram::Bot::Client.new(ENV['BOT_API_TOKEN'], logger: $logger)
      end

      def run
        @bot.listen do |message|
          begin
            params = { message: message }

            case message
            when Telegram::Bot::Types::Message
              BotUtils::LastMessage.new(message.from).unset

              params.merge!({ telegram_user: message.from })

              case message.text
              when /^\/start(?:\s(\w+))?$/
                params.merge!({ token: $1.to_s })
                BotActions::Start.new.call(params)

              when '/help'
                BotActions::Help.new.call(params)

              when '/stop'
                BotActions::Stop.new.call(params)

              when BotKeyboards::MainMenu::BUTTON_RANDOM, '/random'
                params.merge!({ mode: :random })
                BotActions::NextContent.new.call(params)

              when BotKeyboards::MainMenu::BUTTON_TOP, '/top'
                params.merge!({ mode: :top })
                BotActions::NextContent.new.call(params)

              when BotKeyboards::MainMenu::BUTTON_PREMIUM, '/premium'
                BotActions::Premium.new.call(params)

              when BotKeyboards::MainMenu::BUTTON_RATE, '/rate'
                BotActions::Rate.new.call(params)

              when BotKeyboards::MainMenu::BUTTON_INVITE, '/invite'
                BotActions::Invite.new.call(params)

              when BotKeyboards::PremiumMenu::BUTTON_BONUS
                BotActions::PremiumMenu::Bonus.new.call(params)

              when *BotKeyboards::PremiumMenu::PURCHASE_BUTTONS
                message.text.match(BotKeyboards::PremiumMenu::PURCHASE_REGEX)
                params.merge!({ days: $1.to_i, cost: $2.gsub(',', '.').to_f })
                BotActions::PremiumMenu::Purchase.new.call(params)

              when BotKeyboards::PremiumMenu::BUTTON_BACK, '/back', '/cancel'
                BotActions::Back.new.call(params)

              when %r{^/broadcast (?:(?<percent>\d+(?:[\.]\d+)?)% )?(?<message>.+)$}
                # $1 is a percentage
                # $2 is a message
                params.merge!({ percentage: $1&.to_f&./(100), message: $2 })
                BotActions::Admin::Broadcast.new.call(params)

              when '/stats'
                BotActions::Admin::Stats.new.call(params)

              else
                params.merge!({ text: message.text })
                BotActions::GenericMessage.new.call(params)

              end

            when Telegram::Bot::Types::CallbackQuery
              params.merge!({ telegram_user: message.from })

              case message.data
              when BotKeyboards::PostActions::VOTES_REGEX
                params.merge!({ content_id: $1.to_i, action: $2&.to_sym })
                BotActions::PostAction.new.call(params)

              when BotKeyboards::PostActions::ADD_TO_FAVORITE_REGEX
                params.merge!({ content_id: $1.to_i, action: :add_to_favorites })
                BotActions::PostAction.new.call(params)

              else
                # Ignore unknown callback query
              end

            when Telegram::Bot::Types::InlineQuery
              # TODO: todo
            else
              # Ignore unhandled message type
            end

          rescue Exception => e
            # If 'user blocked' error
            #
            if e.class == Telegram::Bot::Exceptions::ResponseError && e.error_code == 403
              Organaizers::ChangeStatus.call({ telegram_user: message&.from, status: :blocked })
            else
              # Rescuing ALL the unhandled errors ONLY in production,
              # in other environments raise
              #
              case ENV['RACK_ENV']
              when 'production'
                $logger.error("#{ e.message } at #{ e.backtrace.join("\n") }")
              else
                raise e
              end
            end
          end
        end
      rescue Telegram::Bot::Exceptions::ResponseError => e
        case e.error_code

        # If 5** error, restart the bot in 5 seconds
        #
        when 500..599
          sleep 5
          run

        # Usually 409 means that someone else is long polling
        when 409
          $logger.warn("Terminated")

        else
          raise e
        end
      end
    end
  end
end
