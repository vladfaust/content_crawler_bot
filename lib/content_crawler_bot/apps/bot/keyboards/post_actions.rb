module ContentCrawlerBot
  class BotKeyboards::PostActions
    VOTES_REGEX = /^vote (\d+) ((?:dis)?like)$/.freeze
    ADD_TO_FAVORITE_REGEX = /^favorite (\d+)$/.freeze

    attr_reader :markup

    def initialize(content_id, favorites: nil, likes: nil, dislikes: nil, ad: nil)
      first_line = []

      first_line << Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{ Settings.emoji_favorites } #{ favorites.to_i }", callback_data: "favorite #{ content_id }") if Settings.features.include?(:favorites)
      if Settings.features.include?(:votes) || Settings.premium_features&.include?(:votes)
        first_line << Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{ Settings.emoji_dislike } #{ dislikes.to_i }", callback_data: "vote #{ content_id } dislike")
        first_line << Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{ Settings.emoji_like } #{ likes.to_i }", callback_data: "vote #{ content_id } like")
      end

      second_line = [
        Telegram::Bot::Types::InlineKeyboardButton.new(text: ad.content['text'], url: ad.content['url'])
      ] if ad

      keyboard = [first_line, second_line].compact

      @markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(
        inline_keyboard: keyboard,
        resize_keyboard: true,
      ) unless keyboard.empty?
    end
  end
end
