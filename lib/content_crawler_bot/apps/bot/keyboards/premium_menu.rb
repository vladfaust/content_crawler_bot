module ContentCrawlerBot
  class BotKeyboards::PremiumMenu
    BUTTON_BACK     = "#{ Settings.emoji_back } #{ I18n.t('bot.buttons.back') }".freeze
    BUTTON_BONUS    = "#{ Settings.emoji_bonus } #{ I18n.t('bot.buttons.bonus', days: I18n.t('dictionary.days', count: Settings.one_time_premium_bonus_days)) }".freeze
    PURCHASE_BUTTONS = Settings.premium_cost.map do |period, cost|
      I18n.t('bot.buttons.purchase', emoji: Settings.emoji_purchase, days: I18n.t('dictionary.days', count: period), currency: Settings.currency_symbol, cost: cost)
    end
    PURCHASE_REGEX = /#{ I18n.t('bot.buttons.purchase_regex', emoji: Settings.emoji_purchase, currency: Settings.currency_symbol) }/

    attr_reader :markup

    def initialize(bonus: false)
      keyboard = []

      keyboard << BUTTON_BONUS if bonus && Settings.one_time_premium_bonus_days
      PURCHASE_BUTTONS.each{ |b| keyboard << b }
      keyboard << BUTTON_BACK

      @markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(
        keyboard: keyboard,
        resize_keyboard: true,
      )
    end
  end
end
