module ContentCrawlerBot
  class BotKeyboards::MainMenu
    BUTTON_RANDOM  = "#{ Settings.emoji_random } #{ I18n.t('bot.buttons.random') }".freeze
    BUTTON_TOP     = "#{ Settings.emoji_top } #{ I18n.t('bot.buttons.top') }".freeze
    BUTTON_PREMIUM = "#{ Settings.emoji_premium } #{ I18n.t('bot.buttons.premium') }".freeze
    BUTTON_RATE    = "#{ Settings.emoji_rate } #{ I18n.t('bot.buttons.rate') }".freeze
    BUTTON_INVITE  = "#{ Settings.emoji_invite } #{ I18n.t('bot.buttons.invite') }".freeze

    attr_reader :markup

    def initialize
      keyboard = [BUTTON_RANDOM]
      keyboard << BUTTON_TOP if Settings.features.include?(:top)
      keyboard << BUTTON_PREMIUM if Settings.premium_features
      keyboard << BUTTON_RATE if Settings.features.include?(:bot_rating)
      keyboard << BUTTON_INVITE if Settings.features.include?(:invites)

      @markup = Telegram::Bot::Types::ReplyKeyboardMarkup.new(
        keyboard: keyboard.each_slice(2).to_a,
        resize_keyboard: true,
      )
    end
  end
end
