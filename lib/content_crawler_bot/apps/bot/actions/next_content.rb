require_relative '_shared'
require 'utils/gif_converter'

module ContentCrawlerBot
  class BotActions::NextContent
    include BotActions::Shared

    def call(params)
      result = Organaizers::NextContent.call(params)
      if result.success?
        response = case result.content.get_type
        when :text
          text = ''
          text = "*#{ result.content.title.gsub(/\n{2,}/, "\n") }*\n\n" if result.content.title
          text += result.content.content

          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: text,
            parse_mode: 'Markdown',
          )
        else
          file = nil

          bot.api.send_chat_action({
            chat_id: params[:telegram_user].id,
            action: "upload_#{ result.content.get_type }",
          })

          input = if result.content.telegram_file_id
            result.content.telegram_file_id
          else
            file = uri_to_file(result.content.content, result.content.get_type)
            mime = IO.popen(["file", "--brief", "--mime-type", file.path], &:read).chomp
            Faraday::UploadIO.new(file.path, mime)
          end

          caption = if result.content.title
            _c = result.content.title.gsub(/\n{2,}/, "\n")
            _c.length > 128 ? "#{ _c[0...125] }..." : _c
          end

          ad = Ad.next_beneath

          response = bot.api.send("send_#{ result.content.get_type }", {
            chat_id: params[:telegram_user].id,
            result.content.get_type => input,
            caption: caption,
            reply_markup: BotKeyboards::PostActions.new(result.content.id,
              likes: result.counters.likes,
              dislikes: result.counters.dislikes,
              favorites: result.counters.favorites,
              ad: ad,
            ).markup,
          })

          File.delete(file.path) if file

          unless result.content.telegram_file_id
            result.content.telegram_file_id = case result.content.get_type
            when :photo
              response['result']['photo'].last['file_id']
            else
              response['result'][result.content.get_type.to_s]['file_id']
            end
            result.content.save_changes
          end

          BotUtils::AdID.new(response['result']['message_id']).set(ad&.id)

          response
        end

        BotUtils::LastMessage.new(params[:telegram_user]).set(response['result']['message_id'])

      elsif result.error == :user_not_found
        BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!

      elsif result.error == :user_banned
        BotActions::Errors::Banned.new(params[:telegram_user]).send!

      elsif result.error == :need_premium
        BotActions::Errors::NeedPremium.new(params[:telegram_user], cause: :top).send!

      elsif result.error == :no_views_left
        BotActions::Errors::NoViewsLeft.new(params[:telegram_user]).send!

      else
        raise Exception.new('Unhandled result failure!')

      end
    end

    def uri_to_file(uri, content_type)
      uri = URI.parse(uri)
      extension = uri.to_s.match(/.+\.(\w+)$/)&.[](1)

      case uri
      when URI::HTTP, URI::HTTPS
        file = Tempfile.new(['content', ".#{ extension }"])
        file.write open(uri.to_s).read
        file.rewind
        file
      else
        File.open(uri.to_s)
      end
    end
  end
end
