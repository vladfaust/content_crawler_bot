require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Help
    include BotActions::Shared

    def call(params)
      text = (I18n.t('bot.messages.help.about.title') + "\n" + I18n.t('bot.messages.help.about.about') + "\n\n" + I18n.t('bot.messages.help.commands_list.title') + "\n" + I18n.t('bot.messages.help.commands_list.random', emoji: Settings.emoji_random) + "\n")
      text += (I18n.t('bot.messages.help.commands_list.top', emoji: Settings.emoji_top) + "\n") if Settings.features.include?(:top)
      text += (I18n.t('bot.messages.help.commands_list.premium', emoji: Settings.emoji_premium) + "\n") if Settings.premium_features
      text += (I18n.t('bot.messages.help.commands_list.rate', emoji: Settings.emoji_rate) + "\n") if Settings.features.include?(:bot_rating)
      text += (I18n.t('bot.messages.help.commands_list.invite', emoji: Settings.emoji_invite) + "\n") if Settings.features.include?(:invites)
      text += (I18n.t('bot.messages.help.commands_list.help') + "\n\n" + I18n.t('bot.messages.help.tips.title') + "\n")
      text += (I18n.t('bot.messages.help.tips.votes', emoji_like: Settings.emoji_like, emoji_dislike: Settings.emoji_dislike) + "\n") if Settings.features.include?(:votes)
      text += (I18n.t('bot.messages.help.tips.favorites', emoji: Settings.emoji_favorites) + "\n") if Settings.features.include?(:favorites)
      text += (I18n.t('bot.messages.help.tips.share'))

      bot.api.send_message(
        chat_id: params[:telegram_user].id,
        text: text,
        parse_mode: 'Markdown',
      )
    end
  end
end
