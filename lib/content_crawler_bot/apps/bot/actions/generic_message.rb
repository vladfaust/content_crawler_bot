require_relative '_shared'

module ContentCrawlerBot
  class BotActions::GenericMessage
    include BotActions::Shared

    def call(params)
      result = Interactors::Users::Find.call(params)
      if result.success?
        BotActions::Errors::DoNotUnderstand.new(params[:telegram_user]).send!
      elsif result.error == :user_not_found
        BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!
      elsif result.error == :user_banned
        BotActions::Errors::Banned.new(params[:telegram_user]).send!
      else
        raise Exception.new('Unhandled result failure!')
      end
    end
  end
end
