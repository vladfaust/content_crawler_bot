require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Rate
    include BotActions::Shared

    def call(params)
      bot.api.send_message(
        chat_id: params[:telegram_user].id,
        text: I18n.t('bot.messages.rate', bot_username: ENV['BOT_USERNAME']),
        parse_mode: 'Markdown',
      )
    end
  end
end
