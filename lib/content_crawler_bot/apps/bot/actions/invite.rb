require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Invite
    include BotActions::Shared

    def call(params)
      result = Interactors::Users::Find.call(params)
      if result.success?
        text = ''
        text += "#{ I18n.t('bot.messages.invite.benefits') } " if Settings.referring_bonus
        text += I18n.t('bot.messages.invite.basic')

        bot.api.send_message(
          chat_id: params[:telegram_user].id,
          text: text,
          parse_mode: 'Markdown',
        )

        text = I18n.t('bot.messages.invite.promo', link: "https://telegram.me/#{ ENV['BOT_USERNAME'] }?start=#{ result.user.referral_token }")

        bot.api.send_message(
          chat_id: params[:telegram_user].id,
          text: text,
          parse_mode: 'Markdown',
        )

      elsif result.error == :user_not_found
        BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!

      elsif result.error == :user_banned
        BotActions::Errors::Banned.new(params[:telegram_user]).send!

      else
        raise Exception.new('Unhandled result failure!')
      end
    end
  end
end
