require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Back
    include BotActions::Shared

    def call(params)
      bot.api.send_message(
        chat_id: params[:telegram_user].id,
        text: I18n.t('bot.messages.back'),
        parse_mode: 'Markdown',
        reply_markup: BotKeyboards::MainMenu.new.markup,
      )
    end
  end
end
