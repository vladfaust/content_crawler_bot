require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Start
    include BotActions::Shared

    def call(params)
      params.merge!({ status: :active, create_if_missing: true })
      result = ContentCrawlerBot::Organaizers::ChangeStatus.call(params)
      if result.success?
        if result.is_new_user
          ContentCrawlerBot::BotActions::Help.new.call(params)

          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: I18n.t('bot.messages.start.welcome', username: params[:telegram_user].first_name),
            parse_mode: 'Markdown',
            reply_markup: BotKeyboards::MainMenu.new.markup,
          )
        else
          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: I18n.t('bot.messages.start.welcome_again', username: params[:telegram_user].first_name),
            parse_mode: 'Markdown',
            reply_markup: BotKeyboards::MainMenu.new.markup,
          )
        end
      elsif result.error == :user_banned
        BotActions::Errors::Banned.new(params[:telegram_user]).send!

      else
        raise Exception.new('Unhandled result failure!')
      end
    end
  end
end
