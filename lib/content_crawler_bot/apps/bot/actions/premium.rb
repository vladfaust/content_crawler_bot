require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Premium
    include BotActions::Shared

    def call(params)
      return BotActions::Errors::PremiumIsDisabled.new(params[:telegram_user]).send! unless Settings.premium_features

      result = Interactors::Users::Find.call(params)
      if result.success?
        text = "#{ I18n.t('bot.messages.premium.about.base', emoji: Settings.emoji_premium) }\n\n"
        %i(unlimited_views top votes favorites).each do |what|
          text += "#{ I18n.t("bot.messages.premium.about.#{ what }", daily_limit: Settings.views_daily_limit, button_top: BotKeyboards::MainMenu::BUTTON_TOP, emoji_favorites: Settings.emoji_favorites, bot_username: ENV['BOT_USERNAME'] ) }\n" if Settings.premium_features&.include?(what)
        end
        text += "\n#{ I18n.t('bot.messages.premium.about.payments') }\n"

        premium_active_until = result.user.premium_until&.strftime("%B %d, %Y") if result.user.premium_active?
        text += "\n#{ I18n.t('bot.messages.premium.about.active_until', until: premium_active_until) }" if premium_active_until

        bot.api.send_message(
          chat_id: params[:telegram_user].id,
          text: text,
          parse_mode: 'Markdown',
          reply_markup: BotKeyboards::PremiumMenu.new(bonus: !result.user.premium_bonus_used).markup,
        )

      elsif result.error == :user_not_found
        BotActions::Errors::UserNotFoundCallback.new(params[:message]).send!

      elsif result.error == :user_banned
        BotActions::Errors::Banned.new(params[:telegram_user]).send!

      else
        raise Exception.new('Unhandled result failure!')

      end
    end
  end
end
