require_relative '_shared'

module ContentCrawlerBot
  class BotActions::Stop
    include BotActions::Shared

    def call(params)
      params.merge!({ status: :blocked, user_set_active: false })
      ContentCrawlerBot::Organaizers::ChangeStatus.call(params)

      bot.api.send_message(
        chat_id: params[:telegram_user].id,
        text: I18n.t('bot.messages.stop'),
        parse_mode: 'Markdown',
        reply_markup: Telegram::Bot::Types::ReplyKeyboardHide.new(hide_keyboard: true))
    end
  end
end
