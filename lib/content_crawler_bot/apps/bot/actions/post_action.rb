require_relative '_shared'

module ContentCrawlerBot
  class BotActions::PostAction
    include BotActions::Shared

    def call(params)
      params.merge!({ user_set_active: false })

      result = case params[:action]
      when :like
        params[:weight] = 1
        ContentCrawlerBot::Organaizers::Vote.call(params)
      when :dislike
        params[:weight] = -1
        ContentCrawlerBot::Organaizers::Vote.call(params)
      when :add_to_favorites
        ContentCrawlerBot::Organaizers::AddToFavorites.call(params)
      else
        raise "Unknown action #{ params[:action] }!"
      end

      if result.success?
        ad = Ad[BotUtils::AdID.new(params[:message].message.message_id).get&.to_i]

        bot.api.edit_message_reply_markup({
          chat_id: params[:telegram_user].id,
          message_id: params[:message].message.message_id,
          reply_markup: BotKeyboards::PostActions.new(result.content.id,
            likes: result.counters.likes,
            dislikes: result.counters.dislikes,
            favorites: result.counters.favorites,
            ad: ad,
          ).markup,
        })

        bot.api.answer_callback_query({
          callback_query_id: params[:message].id,
        })

        if params[:action] != :add_to_favorites && BotUtils::LastMessage.new(params[:telegram_user]).get.to_i == params[:message][:message][:message_id]
          ContentCrawlerBot::BotActions::NextContent.new.call(params)
        end

      elsif result.error == :need_premium
        BotActions::Errors::NeedPremiumCallback.new(params[:message]).send!

      elsif result.error == :repeated_action
        action = case result.action
        when :like
          Settings.emoji_like
        when :dislike
          Settings.emoji_dislike
        when :add_to_favorites
          Settings.emoji_favorites
        end

        BotActions::Errors::RepeatedPostAction.new(params[:message], action: action).send!

      elsif result.error == :user_not_found
        BotActions::Errors::UserNotFoundCallback.new(params[:message]).send!

      elsif result.error == :user_banned
        # BotActions::Errors::Banned.new(params[:telegram_user]).send!

      elsif result.error == :content_not_found
        # We'll just ignore this, thus it's likely to be a bad client

      else
        raise Exception.new('Unhandled result failure!')

      end
    end
  end
end
