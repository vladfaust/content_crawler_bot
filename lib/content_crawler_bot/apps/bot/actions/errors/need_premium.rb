module ContentCrawlerBot
  module BotActions::Errors
    class NeedPremium
      include BotActions::Shared

      def initialize(telegram_user, cause: nil)
        @user = telegram_user
        @cause = cause
      end

      def send!
        text = case @cause
        when :top
          I18n.t('bot.errors.need_premium.top', emoji: Settings.emoji_premium)
        when :votes
          I18n.t('bot.errors.need_premium.votes', emoji: Settings.emoji_premium)
        when :add_to_favorites
          I18n.t('bot.errors.need_premium.add_to_favorites', emoji: Settings.emoji_premium)
        else
          I18n.t('bot.errors.need_premium.generic', emoji: Settings.emoji_premium)
        end

        bot.api.send_message(
          chat_id: @user.id,
          text: text,
          parse_mode: 'Markdown',
        )
      end
    end
  end
end
