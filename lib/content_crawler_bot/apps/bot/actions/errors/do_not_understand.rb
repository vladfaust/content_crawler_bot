module ContentCrawlerBot
  module BotActions::Errors
    class DoNotUnderstand
      include BotActions::Shared

      def initialize(telegram_user)
        @user = telegram_user
      end

      def send!
        bot.api.send_message(
          chat_id: @user.id,
          text: I18n.t('bot.errors.dont_understand', username: @user.first_name),
        )
      end
    end
  end
end
