module ContentCrawlerBot
  module BotActions::Errors
    class RepeatedPostAction
      include BotActions::Shared

      def initialize(message, action: I18n.t('dictionary.done'))
        @message = message
        @action = action
      end

      def send!
        bot.api.answer_callback_query(
          callback_query_id: @message.id,
          text: I18n.t('bot.errors.repeated_action', action: @action),
        )
      rescue Telegram::Bot::Exceptions::ResponseError => e
        raise e unless e.error_code == 400
      end
    end
  end
end
