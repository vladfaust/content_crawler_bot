module ContentCrawlerBot
  module BotActions::Errors
    class NoViewsLeft
      include BotActions::Shared

      def initialize(telegram_user)
        @user = telegram_user
      end

      def send!
        text =  I18n.t('bot.errors.no_views_left.basic', limit: Settings.views_daily_limit)
        text += " #{ I18n.t('bot.errors.no_views_left.invite_friend') }" if Settings.referring_bonus
        text += "\n\n#{ I18n.t('bot.errors.no_views_left.purchase_premium') }" if Settings.premium_features

        bot.api.send_message(
          chat_id: @user.id,
          text: text,
          parse_mode: 'Markdown',
        )
      end
    end
  end
end
