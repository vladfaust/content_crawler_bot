module ContentCrawlerBot
  module BotActions::Errors
    class UserNotFound
      include BotActions::Shared

      def initialize(telegram_user)
        @user = telegram_user
      end

      def send!
        bot.api.send_message(
          chat_id: @user.id,
          text: I18n.t('bot.errors.user_not_found'),
        )
      end
    end
  end
end
