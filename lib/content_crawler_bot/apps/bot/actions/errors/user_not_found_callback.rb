module ContentCrawlerBot
  module BotActions::Errors
    class UserNotFoundCallback
      include BotActions::Shared

      def initialize(message)
        @message = message
      end

      def send!
        bot.api.answer_callback_query(
          callback_query_id: @message.id,
          text: I18n.t('bot.errors.user_not_found_callback_query'),
        )
      rescue Telegram::Bot::Exceptions::ResponseError => e
        raise e unless e.error_code == 400
      end
    end
  end
end
