require_relative '../_shared'

module ContentCrawlerBot
  module BotActions::Admin
    class Stats
      include BotActions::Shared

      def call(params)
        result = Organaizers::Statistics.call(params)
        if result.success?
          s = result.statistics
          formatted_statistics = {
            users_total: s[:users][:count],
            users_active_count: s[:users][:active],
            users_active_percent: ((s[:users][:active].to_f / s[:users][:count]) * 100).round(0),
            users_premium_count: s[:users][:premium],
            users_premium_percent: ((s[:users][:premium].to_f / s[:users][:count]) * 100).round(1),
            users_referrals_count: s[:users][:referrals],
            users_referrals_percent: ((s[:users][:referrals].to_f / s[:users][:count]) * 100).round(1),
            content_total: s[:content][:count],
            content_views_total: s[:content][:views],
            content_views_average_per_content: (s[:content][:views].to_f / s[:content][:count]).round(0),
            content_views_average_per_user: (s[:content][:views].to_f / s[:users][:count]).round(0),
            payments_completed_total: s[:payments][:amount][:total].round(2),
            payments_completed_today: s[:payments][:amount][:today].round(2),
            payments_completed_last_week: s[:payments][:amount][:last_week].round(2),
          }

          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: I18n.t('bot.messages.statistics', { currency: Settings.currency_symbol }.merge(formatted_statistics)),
            parse_mode: 'Markdown',
          )
        elsif result.error == :user_not_found
          BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!
        elsif result.error == :forbidden
          BotActions::Errors::DoNotUnderstand.new(params[:telegram_user]).send!
        else
          raise Exception.new('Unhandled result failure!')
        end
      end
    end
  end
end
