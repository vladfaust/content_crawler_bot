require_relative '../_shared'

module ContentCrawlerBot
  module BotActions::Admin
    class Broadcast
      include BotActions::Shared

      def call(params)
        result = ContentCrawlerBot::Organaizers::Broadcast.call(params)
        if result.success?
          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: I18n.t('bot.messages.broadcast',
              users: result.estimates.users,
              seconds: result.estimates.time),
            parse_mode: 'Markdown',
          )
        elsif result.error == :user_not_found
          BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!
        elsif result.error == :forbidden
          BotActions::Errors::DoNotUnderstand.new(params[:telegram_user]).send!
        else
          raise Exception.new('Unhandled result failure!')
        end
      end
    end
  end
end
