require_relative '../_shared'

module ContentCrawlerBot
  module BotActions::PremiumMenu
    class Bonus
      include BotActions::Shared

      def call(params)
        result = ContentCrawlerBot::Organaizers::AddPremiumBonus.call(params)
        if result.success?
          bot.api.send_message(
            chat_id: params[:telegram_user].id,
            text: I18n.t('bot.messages.premium_bonus', days: I18n.t('dictionary.days', count: Settings.one_time_premium_bonus_days), emoji: Settings.emoji_premium, bot_username: ENV['BOT_USERNAME']),
            parse_mode: 'Markdown',
          )

        elsif result.error == :bonus_used
          BotActions::Errors::BonusUsed.new(params[:telegram_user]).send!

        elsif result.error == :user_not_found
          BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!

        elsif result.error == :user_banned
          BotActions::Errors::Banned.new(params[:telegram_user]).send!

        else
          raise Exception.new('Unhandled result failure!')

        end
      end
    end
  end
end
