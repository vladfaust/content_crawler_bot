require_relative '../_shared'
require 'rqrcode'

module ContentCrawlerBot
  module BotActions::PremiumMenu
    class Purchase
      include BotActions::Shared

      def call(params)
        real_cost = Settings.premium_cost[params[:days]]
        raise InvalidPurchaseArguments unless real_cost&.== params[:cost]

        params.merge!({
          price: real_cost,
          purpose: { 'premium' => params[:days].to_i }
        })

        payment_method = :bitcoinpay
        params.merge!({ payment_method: payment_method})

        result = ContentCrawlerBot::Organaizers::NewPayment.call(params)
        if result.success?
          case payment_method
          when :blockchain
            bot.api.send_message(
              chat_id: params[:telegram_user].id,
              text: I18n.t('bot.messages.payments.instructions.bitcoin', amount: result.payment.amount, timeout: Settings.bitcoin_payment_timeout),
              parse_mode: 'Markdown',
            )

            bot.api.send_message(
              chat_id: params[:telegram_user].id,
              text: "*#{ result.payment.attributes['bitcoin_address'] }*",
              parse_mode: 'Markdown',
            )

            file = generate_payment_qr(result.payment.attributes['bitcoin_address'], result.payment.amount)

            bot.api.send_photo({
              chat_id: params[:telegram_user].id,
              photo: Faraday::UploadIO.new(file.path, "image/png"),
            })

            file&.close
            file&.unlink

          when :bitcoinpay
            bot.api.send_message(
              chat_id: params[:telegram_user].id,
              text: I18n.t('bot.messages.payments.instructions.bitcoinpay', link: result.payment.attributes['payment_link']),
              parse_mode: 'Markdown',
            )
          end

        elsif result.error == :user_not_found
          BotActions::Errors::UserNotFound.new(params[:telegram_user]).send!

        elsif result.error == :user_banned
          BotActions::Errors::Banned.new(params[:telegram_user]).send!

        elsif result.error == :no_bitcoin_addresses
          BotActions::Errors::PaymentError.new(params[:telegram_user]).send!

        else
          raise Exception.new('Unhandled result failure!')

        end
      end

      private

      def generate_payment_qr(address, amount)
        uri = "bitcoin://#{ address }?amount=#{ amount }"
        qr = RQRCode::QRCode.new(uri).as_png(size: 640).to_s

        file = Tempfile.new(['payment-qr', ".png"])
        file.write qr
        file.rewind

        file
      end

      class InvalidPurchaseArguments < StandardError; end
    end
  end
end
