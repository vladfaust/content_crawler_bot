module ContentCrawlerBot
  class BotUtils::AdID
    def initialize(message_id)
      @message_id = message_id
    end

    def get
      $redis.get("messages:#{ @message_id }:ad_id")
    end

    def set(ad_id)
      $redis.set("messages:#{ @message_id }:ad_id", ad_id)
    end

    def unset
      $redis.del("messages:#{ @message_id }:ad_id")
    end
  end
end
