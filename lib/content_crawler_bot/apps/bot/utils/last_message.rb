module ContentCrawlerBot
  class BotUtils::LastMessage
    def initialize(telegram_user)
      @user = telegram_user
    end

    def get
      $redis.get("telegram_users:#{ @user.id }:last_received_message_id")
    end

    def set(message_id)
      $redis.set("telegram_users:#{ @user.id }:last_received_message_id", message_id)
    end

    def unset
      $redis.del("telegram_users:#{ @user.id }:last_received_message_id")
    end
  end
end
