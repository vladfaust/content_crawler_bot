module ContentCrawlerBot::BotActions; end
Dir["#{ __dir__ }/actions/**/*.rb"].each { |file| require file }

module ContentCrawlerBot::BotKeyboards; end
Dir["#{ __dir__ }/keyboards/**/*.rb"].each { |file| require file }

module ContentCrawlerBot::BotUtils; end
Dir["#{ __dir__ }/utils/**/*.rb"].each { |file| require file }
