require 'rake'

module ContentCrawlerBot
  class RakeHelper
    include Rake::DSL

    def self.install_tasks
      new.install
    end

    def install
      namespace :db do
        desc "Run ContentCrawlerBot migrations"
        task :migrate, [:version] do |t, args|
          require "sequel"

          Sequel.extension :migration
          db = Sequel.connect(ENV['DATABASE_URL'])
          db.extension :pg_array,
                       :pg_json

          if args[:version]
            if ENV['RACK_ENV'] == 'production'
              raise 'Can migrate only to latest in production!'
            end
            puts "Migrating to version #{ args[:version] }"
            Sequel::Migrator.run(db, "#{ File.dirname(__FILE__) }/db/migrations", target: args[:version].to_i)
          else
            puts "Migrating to latest"
            Sequel::Migrator.run(db, "#{ File.dirname(__FILE__) }/db/migrations")
          end
        end

        desc 'Clear DB and run ContentCrawlerBot migrations from scratch'
        task :reset do
          if ENV['RACK_ENV'] == 'production'
            raise 'Cannot reset DB in production!'
          end

          require "sequel"
          Sequel.extension :migration
          db = Sequel.connect(ENV['DATABASE_URL'])
          db.extension :pg_array,
                       :pg_json

          puts "Resetting DB"
          Sequel::Migrator.run(db, "#{ File.dirname(__FILE__) }/db/migrations", target: 0)
          Sequel::Migrator.run(db, "#{ File.dirname(__FILE__) }/db/migrations")
        end
      end
    end
  end
end
