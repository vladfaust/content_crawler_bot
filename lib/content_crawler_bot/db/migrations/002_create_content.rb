Sequel.migration do
  change do
    create_table :content do
      primary_key :id
      String :telegram_file_id

      String :title
      Integer :type, null: false
      String :content
      jsonb :attributes

      Integer :views, default: 0
      String :provider_id

      DateTime :created_at
      DateTime :updated_at

      index :type
      index :views
      index :provider_id
    end
  end
end
