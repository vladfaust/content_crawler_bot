Sequel.migration do
  change do
    create_table :votes do
      primary_key :id
      foreign_key :user_id, :users, null: false
      foreign_key :content_id, :content, null: false

      Integer :weight, null: false

      DateTime :created_at
      DateTime :updated_at

      index :user_id
      index :content_id
      index :weight
    end
  end
end
