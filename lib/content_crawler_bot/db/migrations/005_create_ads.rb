Sequel.migration do
  change do
    create_table :ads do
      primary_key :id

      Integer :status, default: 0, null: false
      Integer :type, null: false

      jsonb :content

      DateTime :created_at
      DateTime :updated_at

      index :status
      index :type
    end
  end
end
