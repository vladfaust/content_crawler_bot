Sequel.migration do
  change do
    create_table :users do
      primary_key :id
      Integer :telegram_id, null: false, unique: true
      TrueClass :admin, default: false
      Integer :status, null: false, default: 0

      foreign_key :referrer_id, :users, null: true
      String :referral_token, null: false, unique: true

      DateTime :premium_until
      TrueClass :premium_bonus_used, default: false
      DateTime :unlimited_until

      Integer :views_left, default: 0
      Integer :current_mode, default: 0, null: false
      column :favorites, "integer[]"

      DateTime :created_at
      DateTime :updated_at

      index :telegram_id
      index :referral_token
    end
  end
end
