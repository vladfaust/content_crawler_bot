Sequel.migration do
  change do
    create_table :payments do
      primary_key :id
      foreign_key :user_id, :users, null: true

      jsonb :attributes
      Integer :status, default: 0, null: false
      jsonb :purpose

      Float :amount
      String :currency, default: 'XBT', null: false

      DateTime :active_until
      DateTime :created_at
      DateTime :updated_at

      index :user_id
      index :attributes
      index :purpose
      index :status
      index :active_until
      index :created_at
    end
  end
end
