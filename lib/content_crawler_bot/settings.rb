require 'content_crawler_bot/logic/services/generic_content_provider'

module ContentCrawlerBot
  module Settings
    DEFAULTS = {
      # A list of bot's features enabled by default (no premium needed)
      # A very basic feature is getting random content, cannot be disabled
      #
      # Possible features:
      #   :top        - Ability to sort content by votes - needs :votes to be enabled
      #   :favorites  - Ability to save good content to personal favorites and then post them through inline mode (currently not implemented)
      #   :invites    - Ability to invite friends via referral link
      #   :bot_rating - Ability to rate the bot via Telegram StroreBot & others
      #   :votes      - Ability to vote for/against a content
      #
      features: %i(top invites bot_rating votes),

      # Features which are enabled with premium account and disabled without
      #
      # FEATURES must include these PREMIUM_FEATURES (except :unlimited)
      #
      # Possible features:
      #   :unlimited_views - Premium removes views daily limit
      #   :top             - Premium unlocks :top feature
      #   :favorites       - Premium unlocks :favorites feature (currently not implemented)
      #   :votes           - Premium unlocks :votes feature
      #
      # nil means there is no premium at all
      #
      premium_features: %i(unlimited_views top),

      # Bot's currency in ISO 4217
      #
      currency: 'USD',
      currency_symbol: '$',

      # How much does premium cost in CURRENCY
      #
      # Format: { period_in_days => cost }
      #
      premium_cost: {
        1 => 0.99,
        30 => 3.99,
        365 => 9.99,
      },

      # How many days of premium are gifted when user decides to vote incentively
      #
      # false, nil means no bonus is added => this featues is disabled
      #
      one_time_premium_bonus_days: 1,

      # A daily cap of content views.
      #
      # nil, false means no limit at all
      #
      views_daily_limit: 50,

      # When referring a friend, you (and/or the referral) are given a bonus
      #
      # An invite message should be changed manually
      #
      referring_bonus: {
        unlimited: {
          referral: 1,
          referrer: 1,
        },
        premium: {
          referral: 0,
          referrer: 0,
        }
      },

      # A class which must implement #next_random_content method
      # @see Services::GenericContentProvider
      #
      # @example
      #   Object.const_get(Settings.content_provider_class).new(restricted_ids: +restricted_ids+).next_random_content
      #
      content_provider_class: ContentCrawlerBot::Services::GenericContentProvider,

      ##
      # Emoji settings go next
      #
      emoji_favorites: '⭐️',
      emoji_like: '👍',
      emoji_dislike: '👎',
      emoji_rate: '👾',
      emoji_premium: '👑',
      emoji_back: '🔙',
      emoji_random: '🎲',
      emoji_top: '🔝',
      emoji_invite: '👥',
      emoji_purchase: '💰',
      emoji_bonus: '🎁',

      ##
      # Bitcoin settings
      #

      # Payment window in minutes, after that the address might be reused
      #
      bitcoin_payment_timeout: 15,

      # How many confirmations needed to mark the payment as completed
      #
      # Can be either [Proc] or [Integer]
      # +amount+ passed in block is in BTC
      #
      bitcoin_confirmations_needed: proc do |amount|
        amount > 0.1 ? 3 : 0
      end,

      # How much the value can be smaller than the needed amount (in %)
      #
      bitcoin_mercy: 0.05,

      ##
      # Blockchain settings
      #
      blockchain_callback_path: '/blockchain/callback',
      blockchain_gap: 20,

      ##
      # Bitcoinpay settings
      #
      bitcoinpay_callback_path: '/bitcoinpay/callback',

      ##
      # I18n settings
      #
      i18n_locales_path: nil,
    }

    DEFAULTS.keys.map do |key|
      self.define_singleton_method(key) do
        variable_name = "@@#{ key }".to_sym
        class_variable_defined?(variable_name) ? class_variable_get(variable_name) : class_variable_set(variable_name, DEFAULTS[key])
      end

      self.define_singleton_method(key.to_s + '=') do |value|
        variable_name = "@@#{ key }".to_sym
        class_variable_set(variable_name, value)
      end
    end
  end
end
