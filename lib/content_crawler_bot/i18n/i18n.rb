require 'i18n'
I18n.config.available_locales = :en
I18n.locale = :en
I18n.load_path = ContentCrawlerBot::Settings.i18n_locales_path ? Dir["#{ ContentCrawlerBot::Settings.i18n_locales_path }*.yml"] : Dir["#{ File.dirname(__FILE__) }/locales/*.yml"]
I18n.backend.load_translations
