require 'securerandom'

module ContentCrawlerBot
  module Services
    class GenericContentProvider
      Content = Struct.new(:id, :title, :type, :content)

      def initialize(restricted_ids: nil)
        @restricted_ids = restricted_ids
      end

      def next_random_content
        id = loop do
          id = SecureRandom.hex(8)
          break id unless Array(@restricted_ids).include?(id)
        end
        Content.new(id, 'Some title', :image, 'http://rubyonrails.org/images/imagine.png')
      end
    end
  end
end
