require 'utils/url_builder'
require 'bitcoinpay'

module ContentCrawlerBot
  module Services
    class Bitcoinpay
      CALLBACK_ENDPOINT = Utils::URLBuilder.build(path: Settings.bitcoinpay_callback_path, secure: true)

      def initialize(options = {})
        api_key = options.fetch(:api_key, ENV['BITCOINPAY_API_KEY'])

        @client = ::Bitcoinpay::Client.new(api_key)
      end

      def new_payment(payment_id, amount, currency)
        response = @client.create_new_payment_request(
          settled_currency: 'BTC',
          notify_url: CALLBACK_ENDPOINT,
          price: amount,
          currency: currency,
          reference: {
            payment_id: payment_id,
            secret: ENV['BITCOINPAY_SECRET'],
          },
        )
        raise BitcoinpayClientError.new(response.to_s) unless response&.[]('data')&.[]('address')
        return {
          address: response['data']['address'],
          due_time: response['data']['timeout_time'],
          amount: response['data']['paid_amount'],
          currency: response['data']['paid_currency'],
          payment_url: response['data']['payment_url'],
        }
      end

      def rate(currency = 'USD')
        @client.get_rate(currency)
      end

      class BitcoinpayClientError < StandardError; end
    end
  end
end
