require 'utils/url_builder'
require 'blockchain/api'

module ContentCrawlerBot
  module Services
    class Blockchain
      CALLBACK_ENDPOINT = %w(staging production).include?(ENV['RACK_ENV']) ? Utils::URLBuilder.build(path: Settings.blockchain_callback_path) : "http://example.com#{ Settings.blockchain_callback_path }"

      def initialize(options = {})
        api_key = options.fetch(:api_key, ENV['BLOCKCHAIN_API_KEY'])
        xpub = options.fetch(:xpub, ENV['BLOCKCHAIN_XPUB'])

        @client = ::Blockchain::API::V1::Client.new
        @client_v2 = ::Blockchain::API::V2::Client.new(key: api_key, xpub: xpub)
      end

      def next_address(payment_id, unused_indexes = [])
        unused_index = unused_indexes.sort.first
        callback = "#{ CALLBACK_ENDPOINT }?secret=#{ ENV['BLOCKCHAIN_SECRET'] }&payment_id=#{ payment_id }"

        if unused_index
          response = @client_v2.receive(index: unused_index, callback: callback)
          raise UnexpectedBehaviourError.new(response['message']) unless response['address']
          return {
            address: response['address'],
            index: unused_index,
          }
        elsif @client_v2.gap < Settings.blockchain_gap
          response = @client_v2.receive(callback: callback)
          raise UnexpectedBehaviourError.new(response['message']) unless response['address']
          return {
            address: response['address'],
            index: response['index'],
          }
        else
          nil
        end
      end

      def rate(currency = 'USD')
        rates[currency]['last'].to_f
      end

      def rates
        @client.ticker
      end

      class UnexpectedBehaviourError < StandardError; end
    end
  end
end
