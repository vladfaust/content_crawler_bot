require 'telegram/bot'

module ContentCrawlerBot
  class Jobs::SingleMessage
    @queue = :broadcasting

    SLEEP = 0.1 # s

    def self.perform(telegram_id, message)
      bot = Telegram::Bot::Client.new(ENV['BOT_API_TOKEN'])

      begin
        bot.api.send_message({
          chat_id: telegram_id,
          text: "#{ message }",
          parse_mode: 'Markdown',
        })
      rescue Telegram::Bot::Exceptions::ResponseError => e
        case e.error_code
        when 403
          user = User.find_by_telegram_id(telegram_id)
          user&.blocked!
          user&.save_changes
        else
          raise e
        end
      end

      sleep SLEEP
    end
  end
end
