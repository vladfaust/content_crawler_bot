module ContentCrawlerBot
  class Jobs::RenewDailyViews
    @queue = :low

    def self.perform
      return $logger.info("Views daily limit feature is disabled") unless Settings.views_daily_limit

      counter = 0
      User.all.each do |user|
        next if user.views_left >= Settings.views_daily_limit
        user.views_left = Settings.views_daily_limit
        user.save_changes
        counter += 1
      end

      $logger.info("Renewed :views_left for #{ counter } users")
    end
  end
end
