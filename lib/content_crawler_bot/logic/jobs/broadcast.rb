require 'telegram/bot'
require_relative 'single_message'

module ContentCrawlerBot
  class Jobs::Broadcast
    @queue = :high

    # Broadcasts a message to a given percent of users
    #

    def self.perform(message, percentage = 1.0)
      users_count = 0

      User.all.each do |user|
        if user.active? && rand < percentage.to_f
          users_count += 1
          Resque.enqueue(Jobs::SingleMessage, user.telegram_id, "📢 #{ message }")
        end
      end

      $logger.info("Will broadcast this message to #{ users_count } user(s) in ~#{ Jobs::SingleMessage::SLEEP * users_count } seconds.")
    end
  end
end
