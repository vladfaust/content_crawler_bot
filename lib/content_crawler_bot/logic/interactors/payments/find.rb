module ContentCrawlerBot
  module Interactors::Payments
    class Find
      include Interactor

      # Finds a payment by id
      #
      # @params
      #   payment_id [Integer]
      #
      # @return
      #   payment [Payment]
      #

      def call
        context.payment = Payment[context.payment_id]
        raise PaymentNotFoundError unless context.payment

      rescue PaymentNotFoundError
        context.fail!(error: :payment_not_found)

      end

      class PaymentNotFoundError < StandardError; end
    end
  end
end
