module ContentCrawlerBot
  module Interactors::Payments
    class Create
      include Interactor

      # Creates a new payment
      #
      # @params
      #   user [User]
      #   price [Float] Price in Settings.currency (e.g. USD)
      #   purpose [Hash] Why doing this payment? (e.g. { 'premium' => 30 }, means purchasing premium for 30 days)
      #   payment_method [Symbol] One of PAYMENT_METHODS
      #
      # @return
      #   payment [Payment]
      #

      PAYMENT_METHODS = %i(blockchain bitcoinpay)

      def call
        raise InvalidMethodError unless PAYMENT_METHODS.include?(context.payment_method)

        payment = Payment.create(user: context.user)
        begin
          context.payment = case context.payment_method
          when :blockchain
            next_address = blockchain.next_address(payment.id, Payment.unused_blockchain_indexes)
            raise BlockchainInfoGapError unless next_address&.[](:address)

            rate = blockchain.rate(Settings.currency)
            payment.set({
              amount: (context.price / rate).round(6),
              attributes: {
                'bitcoin_address' => next_address[:address],
                'blockchain_address_index' => next_address[:index],
              },
              active_until: Time.now + Settings.bitcoin_payment_timeout * 60,
              purpose: context.purpose,
            }).save_changes

          when :bitcoinpay
            bitcoinpay_payment = bitcoinpay.new_payment(payment.id, context.price, Settings.currency)
            payment.set({
              amount: bitcoinpay_payment[:amount].to_f,
              currency: bitcoinpay_payment[:currency],
              active_until: Time.at(bitcoinpay_payment[:due_time].to_i),
              attributes: {
                'bitcoin_address' => bitcoinpay_payment[:address],
                'payment_link' => bitcoinpay_payment[:payment_url],
              },
              purpose: context.purpose,
            }).save_changes

          end

        rescue Exception => e
          payment.delete
          raise e
        end

      rescue BlockchainInfoGapError
        $logger.error('BlockchainInfo gap error!')
        context.fail!(error: :no_bitcoin_addresses)

      end

      private

      def bitcoinpay
        @bitcoinpay ||= Services::Bitcoinpay.new
      end

      def blockchain
        @blockchain ||= Services::Blockchain.new
      end

      class BlockchainInfoGapError < StandardError; end
      class InvalidMethodError < StandardError; end
    end
  end
end
