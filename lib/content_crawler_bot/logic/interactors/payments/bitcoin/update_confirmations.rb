module ContentCrawlerBot
  module Interactors::Payments
    module Bitcoin
      class UpdateConfirmations
        include Interactor

        # Updates a Bitcoin payment's confirmations count
        #
        # @params
        #   payment [Payment]
        #   confirmations [Integer]
        #
        # @change
        #   payment
        #

        def call
          context.payment.update_confirmations(context.confirmations)
          context.payment.being_confirmed! if context.payment.created?
          context.payment.save_changes
        end
      end
    end
  end
end
