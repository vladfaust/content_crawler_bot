module ContentCrawlerBot
  module Interactors::Payments
    class Complete
      include Interactor

      # Completes a payment & sends a message to the user
      #
      # @params
      #   payment [Payment]
      #   amount [Float] (Optional) A new amount to set
      #
      # @return
      #   payment [Payment]
      #

      def call
        raise AlreadyCompletedError if context.payment.completed?

        context.payment.amount = context.amount if context.amount
        context.payment.completed!
        context.payment.save_changes

        user = context.payment.user

        if context.payment.purpose&.[]('premium')
          days = context.payment.purpose['premium'].to_i
          user.add_premium_days(days)
          Resque.enqueue(Jobs::SingleMessage, user.telegram_id, I18n.t('bot.notifications.premium_purchased', emoji: Settings.emoji_premium, days: I18n.t('dictionary.days', count: days))) if user.active?
        end

        user.save_changes

      rescue AlreadyCompletedError
        context.fail!(error: :already_completed)

      end

      private

      class AlreadyCompletedError < StandardError; end
      class InvalidPurposeError < StandardError; end
    end
  end
end
