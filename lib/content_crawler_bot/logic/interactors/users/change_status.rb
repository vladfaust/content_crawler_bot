module ContentCrawlerBot
  module Interactors::Users
    class ChangeStatus
      include Interactor

      # Changes user's status
      #
      # @params
      #   user [User]
      #   status [Symbol] A new status to set
      #
      # @change
      #   user
      #

      def call
        context.user.set_status(context.status) unless context.user.banned?
        context.user.save_changes
      end
    end
  end
end
