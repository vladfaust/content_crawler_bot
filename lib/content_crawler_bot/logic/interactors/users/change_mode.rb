module ContentCrawlerBot
  module Interactors::Users
    class ChangeMode
      include Interactor

      # Changes user's content sorting mode
      #
      # @params
      #   user [User]
      #   mode [Symbol] :random or :top, @see User
      #
      # @change
      #   user
      #

      def call
        context.user.set_current_mode(context.mode) if context.mode

        if context.user.get_current_mode == :top
          raise FeatureDisabledError unless Settings.premium_features&.include?(:top) || Settings.features.include?(:top)

          if Settings.premium_features&.include?(:top) && !context.user.premium_active?
            context.user.set_current_mode(:random) && context.user.save_changes
            raise NeedPremiumError
          end
        end

        context.user.save_changes

      rescue NeedPremiumError
        context.fail!(error: :need_premium)

      end

      private

      class FeatureDisabledError < StandardError; end
      class NeedPremiumError < StandardError; end
    end
  end
end
