module ContentCrawlerBot
  module Interactors::Users
    class AddPremiumBonus
      include Interactor

      # Adds bonus days to user
      #
      # @params
      #   user [User]
      #
      # @change
      #   user
      #

      def call
        raise FeatureDisabledError unless Settings.one_time_premium_bonus_days
        raise BonusUsedError if context.user.premium_bonus_used

        context.user.add_premium_days(Settings.one_time_premium_bonus_days)
        context.user.premium_bonus_used = true
        context.user.save_changes

      rescue BonusUsedError
        context.fail!(error: :bonus_used)

      end

      private

      class FeatureDisabledError < StandardError; end
      class BonusUsedError < StandardError; end
    end
  end
end
