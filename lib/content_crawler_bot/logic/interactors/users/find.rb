module ContentCrawlerBot
  module Interactors::Users
    class Find
      include Interactor

      # Finds a User by Telegram ID or optionally creates one if not found in DB
      #
      # @params
      #   telegram_user [Object] Must respond to .id method
      #   create_if_missing [Boolean] (Optional) Whether to create a new user if missing one in DB
      #   token [String] (Optional) Referral token
      #   user_set_active [Boolean] (Optional) Whether to set user's status to active. Defaults to TRUE
      #
      # @change
      #   user's referrer if present
      #
      # @return
      #   user [User]
      #   referrer [User] Only if token is defined
      #   is_new_user [Boolean] Whether is user just created or not
      #

      def call
        context.user = User.find_by_telegram_id(context.telegram_user.id)
        raise UserBannedError if context.user&.banned?

        context.user ? proc do
          user_set_active = context.user_set_active.nil? ? true : context.user_set_active
          return (context.user.active! && context.user.save_changes if user_set_active)
        end.call : (raise UserNotFoundError unless context.create_if_missing)

        context.is_new_user = true
        context.user = User.create(
          telegram_id: context.telegram_user.id,
          views_left: Settings.views_daily_limit,
        )

        if context.token && context.is_new_user
          context.referrer = User.find_by_referral_token(context.token)
          if context.referrer
            context.user.referrer = context.referrer

            %i(unlimited premium).each do |which|
              if Settings.referring_bonus&.[](which)
                to_add = Settings.referring_bonus[which][:referral].to_i
                context.user.send("add_#{ which }_days", to_add) if to_add > 0

                to_add = Settings.referring_bonus[which][:referrer].to_i
                context.referrer.send("add_#{ which }_days", to_add) if to_add > 0
              end
            end

            context.user.save_changes
            context.referrer.save_changes
          end
        end

      rescue UserNotFoundError
        context.fail!(error: :user_not_found)

      rescue UserBannedError
        context.fail!(error: :user_banned)

      end

      class UserBannedError < StandardError; end
      class UserNotFoundError < StandardError; end
    end
  end
end
