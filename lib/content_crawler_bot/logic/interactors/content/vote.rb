module ContentCrawlerBot
  module Interactors::Content
    class VoteContent
      include Interactor

      # Adds a vote associated with the content and returns new counters object
      #
      # @params
      #   user [User]
      #   content [Content]
      #   weight [Integer]
      #

      def call
        raise FeatureDisabledError unless Settings.premium_features&.include?(:votes) || Settings.features.include?(:votes)

        raise NeedPremiumError if Settings.premium_features&.include?(:votes) && !context.user.premium_active?

        previous_vote = Vote.find_for(context.content, context.user)

        if previous_vote
          previous_vote.weight = context.weight
          raise RepeatedVoteError unless previous_vote.column_changed?(:weight)
          previous_vote.save_changes
        else
          Vote.create(
            content: context.content,
            user: context.user,
            weight: context.weight,
          )
        end

      rescue NeedPremiumError
        context.fail!(error: :need_premium)

      rescue RepeatedVoteError
        context.fail!(error: :repeated_action, action: case context.weight
          when 1
            :like
          when -1
            :dislike
          end)

      end

      private

      class FeatureDisabledError < StandardError; end
      class NeedPremiumError < StandardError; end
      class RepeatedVoteError < StandardError; end
    end
  end
end
