module ContentCrawlerBot
  module Interactors::Content
    class Find
      include Interactor

      # Finds a Content by ID
      #
      # @params
      #   content_id [Integer]
      #
      # @return
      #   content [Content]
      #

      def call
        context.content = Content[context.content_id]
        raise NoContentError unless context.content

      rescue NoContentError
        context.fail!(error: :content_not_found)

      end

      class NoContentError < StandardError; end
    end
  end
end
