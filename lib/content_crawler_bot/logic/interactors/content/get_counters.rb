module ContentCrawlerBot
  module Interactors::Content
    class GetCounters
      include Interactor

      # Returns a content's counters
      #
      # @params
      #   content [Content]
      #
      # @return
      #   counters [CountersObject]
      #

      CountersObject = Struct.new(:likes, :dislikes, :favorites)

      def call
        context.counters = CountersObject.new(Vote.count_likes_for(context.content), Vote.count_dislikes_for(context.content), User.having_in_favorites(context.content).count)
      end
    end
  end
end
