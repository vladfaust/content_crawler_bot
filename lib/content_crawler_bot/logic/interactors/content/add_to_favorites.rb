module ContentCrawlerBot
  module Interactors::Content
    class AddToFavorites
      include Interactor

      # Adds the content to the user's favorites
      #
      # @params
      #   content [Content]
      #
      # @change
      #   user
      #

      def call
        raise FeatureDisabledError unless Settings.premium_features&.include?(:favorites) || Settings.features.include?(:favorites)

        raise NeedPremiumError if Settings.premium_features&.include?(:favorites) && !context.user.premium_active?

        raise AlreadyAddedError if context.user.favorites&.include?(context.content.id)

        context.user.add_to_favorites(context.content)
        context.user.save_changes

      rescue NeedPremiumError
        context.fail!(error: :need_premium)

      rescue AlreadyAddedError
        context.fail!(error: :repeated_action, action: :add_to_favorites)

      end

      private

      class FeatureDisabledError < StandardError; end
      class NeedPremiumError < StandardError; end
      class AlreadyAddedError < StandardError; end
    end
  end
end
