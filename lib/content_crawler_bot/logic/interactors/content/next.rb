module ContentCrawlerBot
  module Interactors::Content
    class Next
      include Interactor

      # Returns the next content for this user
      #
      # @params
      #   user [User]
      #
      # @change
      #   user
      #
      # @return
      #   content [Content]
      #

      def call
        raise NoViewsLeftError unless !Settings.views_daily_limit ||
          context.user.views_left > 0 ||
          context.user.unlimited_active? ||
          (Settings.premium_features&.include?(:unlimited_views) && context.user.premium_active?)

        unseen_ids = case context.user.get_current_mode
        when :random
          existing_ids = Content.map(:id)
          seen_ids = get_content_seen_by(context.user)
          existing_ids - seen_ids
        when :top
          contents_with_counters = Content.all_with_counters
          if contents_with_counters.count > 0
            sorted_ids = sort_content(contents_with_counters.dup).map{ |content| content[:id] }
            seen_ids = get_content_seen_by(context.user)
            sorted_ids - seen_ids
          else
            []
          end
        end
        content = unseen_ids.count > 0 ? Content[unseen_ids.sample] : fetch_new_content

        content.views += 1
        content.save_changes
        add_content_seen_by(context.user, content.id)

        context.content = content

        context.user.views_left -= 1 unless context.user.views_left == 0
        context.user.save_changes

      rescue NoViewsLeftError
        context.fail!(error: :no_views_left)

      end

      private

      def fetch_new_content
        fetched = Settings.content_provider_class.new(restricted_ids: Content.map(:provider_id)).next_random_content

        Content.create_with_provider_content(fetched.to_h)
      end

      def sort_content(content)
        av = content.inject(0.0){ |sum, c| sum + c[:views] } / content.count

        content.sort_by! do |c|
          l = c[:likes].to_i
          d = c[:dislikes].to_i
          f = c[:favorites].to_i
          v = c[:views].to_i

          love = (l + f) / (d > 0 ? d : 3)
          involvement = v > 0 ? ((l + f + d) / v) : 0
          confidence = v > 0 ? (v / av) : 0

          love * involvement * confidence
        end

        content.reverse!
      end

      def get_content_seen_by(user)
        $redis.smembers("users:#{ user.id }:seen").map(&:to_i)
      end

      def add_content_seen_by(user, content_id)
        $redis.sadd("users:#{ user.id }:seen", [content_id])
      end

      class NoViewsLeftError < StandardError; end
    end
  end
end
