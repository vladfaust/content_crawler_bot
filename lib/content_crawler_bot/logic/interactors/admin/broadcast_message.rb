module ContentCrawlerBot
  module Interactors::Admin
    class BroadcastMessage
      include Interactor

      # Broadcasts a message
      #
      # @params
      #   user [User] Who's making the request
      #   message [String]
      #   percentage [Float] (Optional) What percent os users to show message to?
      #
      # @return
      #   estimates [EstimationsObject]
      #

      EstimationsObject = Struct.new(:users, :time)

      def call
        raise NeedAdminError unless context.user.admin?

        estimated_users = User.where(status: User.get_status(:active)).count
        estimated_time = estimated_users * Jobs::SingleMessage::SLEEP
        context.estimates = EstimationsObject.new(estimated_users, estimated_time)

        if context.percentage
          Resque.enqueue(Jobs::Broadcast, context.message, context.percentage)
        else
          Resque.enqueue(Jobs::Broadcast, context.message)
        end

      rescue NeedAdminError
        context.fail!(error: :forbidden)

      end

      private

      class NeedAdminError < StandardError; end
    end
  end
end
