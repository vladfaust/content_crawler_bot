module ContentCrawlerBot
  module Interactors::Admin
    class Statistics
      include Interactor

      # Returns the bot's statistics
      #
      # @params
      #   user [User] Who's making the request
      #
      # @return
      #   statistics [Hash]
      #

      def call
        raise NeedAdminError unless context.user.admin?

        context.statistics = {
          users: {
            count: User.count,
            active: User.where(status: User.get_status(:active)).count,
            referrals: User.where('referrer_id IS NOT NULL').count,
            premium: User.where{ premium_until > Time.now }.count,
          },
          content: {
            count: Content.count,
            views: Content.sum(:views),
          },
          payments: {
            count: Payment.count,
            completed: Payment.where(status: Payment.get_status(:completed)).count,
            amount: {
              total: Payment.converted_amount,
              today: Payment.converted_amount(Time.now - Utils::TimeHelpers::T_1_DAY),
              last_week: Payment.converted_amount(Time.now - Utils::TimeHelpers::T_1_WEEK),
            }
          }
        }

      rescue NeedAdminError
        context.fail!(error: :forbidden)

      end

      private

      class NeedAdminError < StandardError; end
    end
  end
end
