require 'attribute_enum'

module ContentCrawlerBot
  class Ad < Sequel::Model(:ads)
    include AttributeEnum
    enum :type, %i(beneath)
    enum :status, %i(shown hidden)

    def self.next_beneath
      where(type: get_type(:beneath)).and(status: get_status(:shown)).all.sample
    end
  end
end
