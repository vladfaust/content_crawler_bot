require 'attribute_enum'
require 'securerandom'
require 'utils/time_helpers'

module ContentCrawlerBot
  class User < Sequel::Model(:users)
    include AttributeEnum
    enum :status, %i(active blocked banned)
    enum :current_mode, %i(random top)

    def admin?
      admin
    end

    ##
    # Telegram

    def self.find_by_telegram_id(telegram_id)
      where(telegram_id: telegram_id).first
    end

    ##
    # Referral system
    #

    many_to_one :referrer, class: self, key: :referrer_id

    def self.find_by_referral_token(referral_token)
      where(referral_token: referral_token).first
    end

    def generate_unique_referral_token
      self.referral_token = loop do
        token = SecureRandom.hex(16)
        break token if User.find_by_referral_token(token).nil?
      end
    end

    ##
    # Premium & unlimited
    #

    def premium_active?
      self.premium_until&.>(Time.now)
    end

    def add_premium_days(count)
      self.premium_until ||= Time.now
      self.premium_until += Utils::TimeHelpers::T_1_DAY * count
    end

    def unlimited_active?
      self.unlimited_until&.>(Time.now)
    end

    def add_unlimited_days(count)
      self.unlimited_until ||= Time.now
      self.unlimited_until += Utils::TimeHelpers::T_1_DAY * count
    end

    ##
    # Favorites
    #

    def self.having_in_favorites(content)
      self.where("favorites @> array[#{ content.id }]::integer[]")
    end

    def add_to_favorites(content)
      will_change_column(:favorites)
      (self.favorites ||= []).push(content.id)
    end

    ##
    # Hooks
    #

    def before_create
      generate_unique_referral_token
    end
  end
end
