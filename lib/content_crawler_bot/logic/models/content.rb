require 'attribute_enum'

module ContentCrawlerBot
  class Content < Sequel::Model(:content)
    include AttributeEnum
    enum :type, %i(audio document sticker video voice photo text)

    def self.find_by_provider_id(id)
      where(provider_id: id.to_s).first
    end

    def self.create_with_provider_content(content = {})
      self.create(
        provider_id: content[:provider_id],
        title: content[:title],
        type: self.get_type(content[:type]),
        content: content[:content],
      )
    end

    # @return
    #   [{ id: 1, likes: 7, dislikes: 8, favorites: 10, views: 42 }, { ... }]
    #
    def self.all_with_counters
      $db["
        SELECT
          COUNT(CASE votes.weight WHEN 1 THEN 1 ELSE NULL END) as likes,
          COUNT(CASE votes.weight WHEN -1 THEN 1 ELSE NULL END) as dislikes,
          content.views AS views,
          content.id AS id
        FROM
          content
        LEFT OUTER JOIN
          votes ON votes.content_id = content.id
        GROUP BY
          content.id
        ORDER BY
          content.id ASC
      "].all
    end
  end
end
