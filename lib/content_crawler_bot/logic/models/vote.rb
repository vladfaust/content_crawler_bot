module ContentCrawlerBot
  class Vote < Sequel::Model(:votes)
    many_to_one :content
    many_to_one :user

    def self.count_likes_for(content)
      self.where(content: content).and(weight: 1).count
    end

    def self.count_dislikes_for(content)
      self.where(content: content).and(weight: -1).count
    end

    def self.find_for(content, user)
      self.where(content: content).and(user: user).first
    end
  end
end
