require 'attribute_enum'
require 'content_crawler_bot/logic/services/blockchain'

module ContentCrawlerBot
  class Payment < Sequel::Model(:payments)
    include AttributeEnum
    many_to_one :user

    enum :status, %i(created being_confirmed completed)

    def self.find_active_for(user)
      self.where(user: user).and{ active_until > Time.now }
    end

    def update_confirmations(confirmations)
      will_change_column(:attributes)
      self.attributes['confirmations'] = confirmations.to_i
    end

    def self.unused_blockchain_indexes
      $db["
        SELECT *
        FROM
        (
          SELECT
            attributes->>'blockchain_address_index' AS blockchain_address_index,
            MAX(active_until)
          FROM
            payments
          WHERE
            status = #{ self.get_status(:created) }
          GROUP BY
            blockchain_address_index
        ) as q
        WHERE
          q.max < NOW()
      "].map(:blockchain_address_index)
    end

    def self.converted_amount(since = nil)
      btc_rate = Services::Bitcoinpay.new.rate(Settings.currency)

      ds = self.where(status: self.get_status(:completed))
      ds = ds.and{ updated_at > since } if since

      result = ds.inject(0.0) do |sum, payment|
        case payment.currency
        when 'XBT', 'BTC'
          sum += btc_rate * payment.amount
        end
      end

      result
    end
  end
end
