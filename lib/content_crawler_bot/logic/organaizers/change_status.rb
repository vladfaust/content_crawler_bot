module ContentCrawlerBot
  class Organaizers::ChangeStatus
    include Interactor::Organizer

    organize Interactors::Users::Find, Interactors::Users::ChangeStatus
  end
end
