module ContentCrawlerBot
  class Organaizers::AddToFavorites
    include Interactor::Organizer

    organize Interactors::Users::Find, Interactors::Content::Find, Interactors::Content::AddToFavorites, Interactors::Content::GetCounters
  end
end
