module ContentCrawlerBot
  class Organaizers::Vote
    include Interactor::Organizer

    organize Interactors::Users::Find, Interactors::Content::Find, Interactors::Content::VoteContent, Interactors::Content::GetCounters
  end
end

