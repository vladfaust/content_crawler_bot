module ContentCrawlerBot
  class Organaizers::Statistics
    include Interactor::Organizer

    organize ContentCrawlerBot::Interactors::Users::Find, ContentCrawlerBot::Interactors::Admin::Statistics
  end
end
