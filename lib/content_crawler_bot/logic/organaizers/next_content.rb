module ContentCrawlerBot
  class Organaizers::NextContent
    include Interactor::Organizer

    organize Interactors::Users::Find, Interactors::Users::ChangeMode, Interactors::Content::Next, Interactors::Content::GetCounters
  end
end
