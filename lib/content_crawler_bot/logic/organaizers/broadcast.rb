module ContentCrawlerBot
  class Organaizers::Broadcast
    include Interactor::Organizer

    organize ContentCrawlerBot::Interactors::Users::Find, ContentCrawlerBot::Interactors::Admin::BroadcastMessage
  end
end
