module ContentCrawlerBot
  class Organaizers::AddPremiumBonus
    include Interactor::Organizer

    organize Interactors::Users::Find, Interactors::Users::AddPremiumBonus
  end
end
