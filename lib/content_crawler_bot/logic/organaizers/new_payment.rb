module ContentCrawlerBot
  class Organaizers::NewPayment
    include Interactor::Organizer

    organize Interactors::Users::Find, ContentCrawlerBot::Interactors::Payments::Create
  end
end
