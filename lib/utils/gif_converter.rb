module Utils
  class GifConverter
    # Converts a file to .gif, and returns a new path
    #
    def video_to_gif(input_file_path, fps: 20, scale: 320, optimize: false)
      gif_path = "#{ input_file_path }.gif"
      $logger.debug("Starting conversion of #{ input_file_path } (#{ (File.size(input_file_path).to_f / 10 ** 3).round(1) } kB)")
      `ffmpeg -v quiet -i "#{ input_file_path }" -pix_fmt rgb8 -r #{ fps } "#{ gif_path }"`
      $logger.debug("Converted to #{ gif_path } (#{ (File.size(gif_path).to_f / 10 ** 3).round(1) } kB)")
      if optimize
        $logger.debug("Optimizing #{ gif_path }")
        `convert -layers Optimize "#{ gif_path }" "#{ gif_path }"`
        $logger.debug("Optimized to #{ (File.size(gif_path).to_f / 10 ** 3).round(1) } kB")
      end
      gif_path
    end

    # Does the same but deletes the original file
    #
    def video_to_gif!(input_file_path, options = {})
      gif_path = video_to_gif(input_file_path, options)
      File.delete(input_file_path)
      gif_path
    end
  end
end
