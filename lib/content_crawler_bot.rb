require 'content_crawler_bot/version'

require 'content_crawler_bot/settings'
require 'content_crawler_bot/i18n/i18n'

module ContentCrawlerBot
  require 'utils/sequel'
  $db = Utils::SequelConnection.new

  Sequel::Model.plugin :dirty
  Sequel::Model.plugin :timestamps
  Sequel::Model.plugin :validation_helpers
  Sequel::Model.plugin :defaults_setter

  require 'utils/redis'
  $redis = Utils::Redis.new
  Resque.redis = Utils::Redis.single_connection

  require 'interactor'
  module Interactors; end
  module Organaizers; end

  module Services; end
  module Jobs;end
end

require 'content_crawler_bot/logic/logic'
